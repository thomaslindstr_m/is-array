// ---------------------------------------------------------------------------
//  is-array
//
//  array type checker
//  - variable: Variable to be checked
// ---------------------------------------------------------------------------

    var toString = Object.prototype.toString;
    var TAG_ARRAY = '[object Array]';

    var isArray = Array.isArray || function isArray(variable) {
        return toString.call(variable) === TAG_ARRAY;
    };

    module.exports = isArray;
